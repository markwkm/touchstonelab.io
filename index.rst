==================
 Touchstone Tools
==================

.. image:: ts-tools.png
   :width: 75px

Tools for capturing system and database statistics, as well as tools for
visualizing that data.

Download current releases:

.. list-table::
   :header-rows: 1

   * - Version
     - Download
     - Checksum
     - Release date
   * - 0.9.2
     - `.AppImage <download/ts-tools-0.9.2-x86_64.AppImage>`_
     - `md5 <download/ts-tools-0.9.2-x86_64.AppImage.md5>`__
       `sha256  <download/ts-tools-0.9.2-x86_64.AppImage.sh256>`__
     - 2024-11-01
   * -
     - `.xz <download/touchstone-tools-0.9.2.tar.xz>`_
     - `md5 <download/touchstone-tools-0.9.2.tar.xz.md5>`__
       `sha256  <download/touchstone-tools-0.9.2.tar.xz.sh256>`__
     -

`View the source code on-line.
<https://gitlab.com/touchstone/touchstone-tools>`_
